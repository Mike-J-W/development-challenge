import { render, waitFor, screen, within } from '@testing-library/react';
import App from './App';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

test('only users with an application render with Pay button', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const tableRows = screen.getAllByRole('row').slice(1);
  const moneyRegex = />\$\d+\.\d\d</g
  tableRows.forEach(row => {
    // Count cells with only a monetary value
    const matches = moneyRegex[Symbol.matchAll](row.innerHTML);
    const matchesCount = Array.from(matches).length;
    expect(matchesCount).toBeLessThan(3);
    // If the row has only one match, then it should have a request but no payment
    // Such a row should have the Pay button. Others should not.
    const button = within(row).queryByRole('button');
    if(matchesCount === 1) {
      expect(button.innerHTML).toContain('Pay');  
    } else {
      expect(button).toBeNull();  
    }
  });  
});
